var io = require('socket.io'),
  connect = require('connect'),
  chatter = require('chatter'),
  http = require('http'),
  path = require('path'),
  fs = require('fs');

var app = connect().use(connect.static('public')).listen(process.env.PORT || 5000);
var chat_room = io.listen(app);
chatter.set_sockets(chat_room.sockets);

chat_room.sockets.on('connection', function (socket) {
    chatter.connect_chatter({
        socket: socket
    });
});